*** Settings ***
Library           Collections
Library           SeleniumLibrary
Library           OperatingSystem
Library           Lib/PerformanceLibrary.py

*** Variables ***
${USRNAME}        mxsrperformance@cemex.onmicrosoft.com
${USRPASSWD}      Chema3202
&{MX}             URL=https://uscldyndbp01.crm.dynamics.com    PAIS=MEX    MENU=Ventas    SUBMENU=Oportunidades    OPPORTUNITY=HTTP WATCH TEST    FASE2=2. Indagar Oportunidad    SALESORG=1670
...               FASE3=3. Presentar Propuesta de Valor    MOSTRARPROD=Mostrar Productos    PERFIL=MX Sr Performance USCLDYNDBP01    # Vars for Mexico
${COUNTRY}        &{MX}    # Nota: Seleccionar EG para EGIPTO y MX para MEXICO
${pmenu}          &{COUNTRY}[MENU]
${psubmenu}       &{COUNTRY}[SUBMENU]
${pOpportunity}    &{COUNTRY}[OPPORTUNITY]
${pMostrarProd}    &{COUNTRY}[MOSTRARPROD]
${pPerfil}        &{COUNTRY}[PERFIL]
${pPais}          &{COUNTRY}[PAIS]
${URL}            &{COUNTRY}[URL]    # PRD Environment MX=https://uscldyndbp01.crm.dynamics.com | https://eucldyndbp01.crm4.dynamics.com

*** Test Cases ***
TestCRMMob_MX_MobileIphone
    Set Chrome Desired Capabilities
    MobileWorkFlow

*** Keywords ***
Set Chrome Desired Capabilities
    [Documentation]    Create the desired capabilities object with which to instantiate the Chrome browser.
    ${dc}    Evaluate    sys.modules['selenium.webdriver'].DesiredCapabilities.CHROME    sys, selenium.webdriver
    ${MobileEmulation}    Create Dictionary    deviceName    iPhone 6/7/8
    ${experimental_options}    Create Dictionary    mobileEmulation    ${MobileEmulation}
    #Set To Dictionary    ${experimental_options}    mobileEmulation    ${MobileEmulation}
    Set To Dictionary    ${dc}    chromeOptions    ${experimental_options}
    Set Global Variable    ${DESIRED_CAPABILITIES}    ${dc}

MobileWorkFlow
    LoginM
    CentrodeVentas
    SearchOpportunity
    DisplayContact
    DisplayObra
    DisplayCliente
    DisplayQuote
    Logout

LoginM
    #Initialize Performance Test
    ###    Load Inital Page
    transaction start
    Open Browser    about:blank    browser=chrome    desired_capabilities=${DESIRED_CAPABILITIES}
    Go To    ${URL}
    ${status} =    Run Keyword and Return Status    Wait until page contains element    //input[@type="text" and @name="loginfmt"]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedInitialPage    #FailLoadingInitialPage
    transaction end    CRMMob_${pPais}_00_LoadInitialPage    ${status}
    Comment    Capture Page Screenshot
    ###    EnterUserName
    transaction start
    transaction start
    Input Text    //input[@type="text" and @name="loginfmt"]    ${USRNAME}
    Press Key    //input[@type="text" and @name="loginfmt"]    \\13
    sleep    1s
    ${status} =    Run Keyword and Return Status    Wait Until Element Is Visible    //input [@name="passwd"]    timeout=60    #//input[@type="submit" and @value="Sign in"]
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedUserName    #FailedUserName
    transaction end    CRMMob_${pPais}_01.01_EnterUserName    ${status}
    Comment    Capture Page Screenshot
    ###    EnterUserPassword
    transaction start
    Input Text    //input [@name="passwd"]    ${USRPASSWD}
    Press Key    //input[@type="text" and @name="loginfmt"]    \\13
    sleep    1s
    ${status} =    Run Keyword and Return Status    Wait Until Element Is Visible    //input[@id="idBtn_Back"]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedPassword    #FailedPassword
    transaction end    CRMMob_${pPais}_01.02_EnterUserPassword    ${status}
    Comment    Capture Page Screenshot
    ###    EnterYesButton
    Sleep    2s
    transaction start
    Press Key    //input[@id="idBtn_Back"and@value="No"]    \\13
    sleep    1s
    Select frame    //iframe [@id="AppLandingPage"]
    ${status} =    Run Keyword and Return Status    Wait until page contains element    //div[@title="Centro de ventas"]    timeout=60
    Comment    Sleep    1s
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedEnterYesButton
    transaction end    CRMMob_${pPais}_01.03_EnterYesButton    ${status}
    Comment    Capture Page Screenshot
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedLogin    #FailedLogin
    transaction end    CRMMob_${pPais}_01_Login    ${status}
    sleep    1s
    Comment    Capture Page Screenshot

CentrodeVentas
    Comment    Select frame    //iframe [@id="AppLandingPage"]
    transaction start
    click element    //div[@title="Centro de ventas"]
    sleep    1s
    ${status} =    Run Keyword and Return Status    Wait until page contains element    //div[@data-id="MscrmControls.Containers.DashboardControl-Chart1"]
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedCentrodeVentas
    transaction end    CRMMob_${pPais}_02_CentrodeVentas    ${status}

SearchOpportunity
    Set Focus to Element    //button[@data-id="searchLauncher"]
    ###    SearchOportunidad
    transaction start
    Click element    //button[@data-id="searchLauncher"]
    ${status} =    Run Keyword and Return Status    Wait Until Element Is Visible    //input[@data-id="categorized-search-text-input"]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedSearch    #FailedSearch
    Set Focus To Element    //input[@data-id="categorized-search-text-input"]
    Input Text    //input[@data-id="categorized-search-text-input"]    ${pOpportunity}
    Press Key    //input[@data-id="categorized-search-text-input"]    \\13
    ${status} =    Run Keyword and Return Status    Wait Until Element Is Visible    //ul[@id="MscrmControls.Grid.GridControl-opportunity-MscrmControls.Grid.GridControl.opportunity-GridList"]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedSearch    #FailedSearch
    set focus to element    //ul[@id="MscrmControls.Grid.GridControl-opportunity-MscrmControls.Grid.GridControl.opportunity-GridList"]
    Click element    //ul[@id="MscrmControls.Grid.GridControl-opportunity-MscrmControls.Grid.GridControl.opportunity-GridList"]
    Comment    Capture Page Screenshot    #antes de la validacion
    sleep    2s
    ${status} =    Run Keyword and Return Status    Wait Until Element Is Visible    //div[@data-id="MscrmControls.Containers.ProcessBreadCrumb-stageNameContainer_c5162ce0-395e-4fa0-8b65-8a0cb21dedea"]    timeout=60    #//li[@aria-label="Entidad: Oportunidad, fase: 4. Cerrar Venta, estado: Completed. "]
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedSearch    #FailedSearch
    sleep    3s
    Comment    Capture Page Screenshot
    ${status} =    Run Keyword and Return Status    Wait Until Element Is Visible    //li[@aria-label="Entidad: Oportunidad, fase: 3. Presentar Propuesta de Valor, estado: Completed. "]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedSearch    #FailedSearch
    transaction end    CRMMob_${pPais}_03_SearchOpportunity    ${status}
    sleep    3s
    Click element    //li[@aria-label="Entidad: Oportunidad, fase: 3. Presentar Propuesta de Valor, estado: Completed. "]
    sleep    3s
    Set Focus to Element    //button[@title="Mover a la izquierda"]    #Mover el boton a la izq hasta que aparezca ph2
    double Click Element    //button[@title="Mover a la izquierda"]

DisplayContact
    Sleep    3s
    ###    DisplayDatosContacto
    transaction start
    Click Element    //label [@data-id="header_process_parentcontactid_1.fieldControl-LookupResultsDropdown_parentcontactid_selected_tag_text"]    #//*[@id="header_process_parentcontactid_1-header_process_parentcontactid_1-header_process_parentcontactid_1.fieldControl-LookupResultsDropdown_parentcontactid_17_selected_tag_text_0"]
    Wait until page contains element    //*[@data-id="CONTACT_INFORMATION"]    timeout=30
    Comment    Capture Page Screenshot
    Comment    Displays: Datos de Contacto
    ${status} =    Run Keyword and Return Status    Wait Until Element Is Visible    //*[@data-id="CONTACT_INFORMATION"]    timeout=120
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedDisplayContact    #FailedDisplayContact
    transaction end    CRMMob_${pPais}_04.01_DisplayDatosContacto    ${status}
    Comment    Capture Page Screenshot
    Sleep    2s
    Go Back
    Wait until page contains element    //iframe[@id="authFrame"]    timeout=60

DisplayObra
    sleep    3s
    ###    DisplayDatosObra
    transaction start
    Click Element    //span [@data-id="header_process_cmx_jobsite_1.fieldControl-crmSymbolFont_selectedRecords_entity-symbol"]
    ${status} =    Run Keyword and Return Status    Wait until page contains element    //div [@data-id="Sales_Data_jobsite_container"]    timeout=120    #//td[@tabindex='${pSalesOrg}' and @title='Checkbox']
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedDisplayDatosObra    #FailedDisplayDatosObra
    transaction end    CRMMob_${pPais}_04.02_DisplayDatosObra    ${status}
    Comment    Capture Page Screenshot
    Run Keyword If    '&{COUNTRY}[PAIS]'=="EGY"    CloseErrorMsg
    #Run Keyword If    //div[id="ErrorTitle"]    CloseErrorMsg
    Sleep    1s
    ###    DisplayDatosCuenta
    transaction start
    Double Click Element    //div [@data-id="Sales_Data_jobsite_container"]    #//td[@tabindex='${pSalesOrg}' and @title='Checkbox']
    ${status} =    Run Keyword and Return Status    Wait until page contains element    //h1[contains(@title, 'Sales Data')]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedDisplayDatosCuenta    FailedDisplayDatosCuenta
    transaction end    CRMMob_${pPais}_04.03_DisplayDatosCuenta    ${status}
    Comment    Capture Page Screenshot
    Sleep    1s
    Go Back
    Wait until page contains element    //div [@data-id="Sales_Data_jobsite_container"]    timeout=60
    Sleep    1s
    Go Back
    Wait until page contains element    //iframe[@id="authFrame"]    timeout=120

DisplayCliente
    sleep    3s
    ###    DisplayDatosCliente
    transaction start
    Click Element    //label [@data-id="header_process_parentaccountid_1.fieldControl-LookupResultsDropdown_parentaccountid_selected_tag_text"]
    #Wait Until Page Contains Element    //td[@id="datos generales del cliente_c"]    timeout=120
    ${status}    Run Keyword and Return Status    Wait Until Element Is Visible    //div [@data-id="tabpanel-tab_general"]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedDisplayDatosCliente    #FailedDisplayDatosCliente
    transaction end    CRMMob_${pPais}_04.04_DisplayDatosCliente    ${status}
    Comment    Capture Page Screenshot
    sleep    2s
    Go Back
    #Wait until element is visible    //span[text()="3. Presentar Propuesta de Valor"]    timeout=60
    Wait until page contains element    //iframe[@id="authFrame"]    timeout=60

DisplayQuote
    #Desplazamiento a la derecha
    Click Element    //button[@title="Mover a la derecha"]
    sleep    2s
    Click element    //li [@aria-label="Entidad: Oportunidad, fase: 3. Presentar Propuesta de Valor, estado: Completed. "]
    Sleep    3s
    ###    DisplayQuote
    transaction start
    Click Element    //label [@data-id="header_process_cmx_quote.fieldControl-LookupResultsDropdown_cmx_quote_selected_tag_text"]    # Click en Cotizacion is HTTP Watch
    ${status} =    Run Keyword and Return Status    Wait Until Element Is Visible    //li [@aria-label="Más pestañas"]    timeout=60    #//span [@aria-label="Mostrar Productos"]
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedDisplayQuote    #FailedDisplayQuote
    transaction end    CRMMob_${pPais}_04.05_DisplayQuote    ${status}
    Comment    Capture Page Screenshot
    Comment    Displays: Datos de la Cotizacion
    Sleep    2s
    ###    DisplayProducts
    transaction start
    Click Element    //li [@aria-label="Más pestañas"]    #//span [@aria-label="Mostrar Productos"]
    Comment    Sleep    5s
    Comment    Capture Page Screenshot
    ${status} =    Run Keyword and Return Status    Wait until element is visible    //li [@aria-label="PRODUCTOS COTIZADOS"]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedDisplayProducts    #FailedDisplayProducts
    Click element    //li [@aria-label="PRODUCTOS COTIZADOS"]    #//span[text()=" Mostrar Productos "]
    ${status} =    Run Keyword and Return Status    Wait until element is visible    //label [@data-id="quotelines-ListItemLabel_cmx_item_72192231-a415-e811-8115-3863bb348b08"]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction FailedDisplayProducts    #FailedDisplayProducts
    transaction end    CRMMob_${pPais}_04.06_DisplayProducts    ${status}
    Sleep    1s
    Comment    Capture Page Screenshot
    Go Back

Logout
    ###    Logout
    transaction start
    Click Element    //li[@id="OverflowButton_buttoncrm_header_global"]
    Wait until page contains element    //button [@data-id="userInformationLauncher"]    timeout=5
    Sleep    1s
    Click element    //button [@data-id="userInformationLauncher"]
    Comment    Capture Page Screenshot
    #Wait until page contains element    //*[@id="login_workload_logo_text"]    timeout=60
    Wait until page contains element    //span[text()="Cerrar sesión"]    timeout=5
    Sleep    1s
    Click element    //span[text()="Cerrar sesión"]
    ${status} =    Run Keyword and Return Status    Wait until page contains element    //*[@id="login_workload_logo_text"]    timeout=60
    Run Keyword If    '${status}'!='True'    Fail Transaction    "FailedLogout"
    transaction end    CRMMob_${pPais}_05_Logout    ${status}
    Comment    Capture Page Screenshot
    close browser
    Run    taskkill /IM chromedriver.exe /F
    Run    taskkill /IM chrome.exe /F

Fail Transaction ${pfailure}
    Capture Page Screenshot
    Fail    ${pfailure}
    Logout
