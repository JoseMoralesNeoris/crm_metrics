//   *****************************************************************************************************************************************
//   ****   PLEASE NOTE: This is a READ-ONLY representation of the actual script. For editing please press the "Develop Script" button.   ****
//   *****************************************************************************************************************************************

Action()
{
	lr_start_transaction("CRM_PRD_MX_00_LoadInitPage");
	truclient_step("1", "Navigate to 'http://uscldyndbp01.crm.dynamics.com/'", "snapshot=Action_1.inf");
	lr_end_transaction("CRM_PRD_MX_00_LoadInitPage",0);
	truclient_step("2", "Resize browser window to 1024 px / 768 px", "snapshot=Action_2.inf");
	lr_start_transaction("CRM_PRD_MX_01_Login");
	truclient_step("3", "Wait 1 seconds", "snapshot=Action_3.inf");
	truclient_step("4", "Wait until Sign in heading exists", "snapshot=Action_4.inf");
	lr_start_transaction("CRM_PRD_MX_01.01_EnterUserName");
	truclient_step("5", "Click on Sign in Can�t access... emailbox", "snapshot=Action_5.inf");
	truclient_step("6", "Type mxsrperformance@cemex.onmicrosoft.com in Sign in Can�t access... emailbox", "snapshot=Action_6.inf");
	lr_end_transaction("CRM_PRD_MX_01.01_EnterUserName",0);
	lr_start_transaction("CRM_PRD_MX_01.02_ClickNEXTButton");
	truclient_step("7", "Click on Next button", "snapshot=Action_7.inf");
	truclient_step("8", "Mouse down on Enter password Forgot... passwordbox", "snapshot=Action_8.inf");
	{
		truclient_step("8.1", "Press Cancel in authentication dialog", "snapshot=Action_8.1.inf");
	}
	lr_end_transaction("CRM_PRD_MX_01.02_ClickNEXTButton",0);
	lr_start_transaction("CRM_PRD_MX_01.03_EnterPassword");
	truclient_step("9", "Sign in", "snapshot=Action_9.inf");
	{
		truclient_step("9.1", "Click on Enter password Forgot... passwordbox", "snapshot=Action_9.1.inf");
		truclient_step("9.2", "Type ********* in Enter password Forgot... passwordbox", "snapshot=Action_9.2.inf");
		truclient_step("9.3", "Click on Sign in button", "snapshot=Action_9.3.inf");
	}
	lr_end_transaction("CRM_PRD_MX_01.03_EnterPassword",0);
	lr_start_transaction("CRM_PRD_MX_01.04_EnterSignInButton");
	truclient_step("10", "Click on Yes button", "snapshot=Action_10.inf");
	lr_end_transaction("CRM_PRD_MX_01.04_EnterSignInButton",0);
	truclient_step("11", "Type mxsrperformance@cemex.onmicrosoft.com in Sign in Can�t access... emailbox", "snapshot=Action_11.inf");
	truclient_step("12", "Press key Enter on Sign in Can�t access... emailbox", "snapshot=Action_12.inf");
	truclient_step("13", "Wait 1 seconds", "snapshot=Action_13.inf");
	truclient_step("14", "Wait until mxsrperformance@cemex.onmicrosoft.com... passwordbox exists", "snapshot=Action_14.inf");
	truclient_step("15", "Type ********* in mxsrperformance@cemex.onmicrosoft.com... passwordbox", "snapshot=Action_15.inf");
	truclient_step("16", "Click on Sign in button", "snapshot=Action_16.inf");
	truclient_step("17", "Click on Yes button", "snapshot=Action_17.inf");
	truclient_step("18", "Wait 1 milliseconds", "snapshot=Action_18.inf");
	lr_end_transaction("CRM_PRD_MX_01_Login",0);
	truclient_step("23", "Resize browser window to 1600 px / 900 px", "snapshot=Action_23.inf");
	truclient_step("24", "Click on No volver a mostrar esta... checkbox", "snapshot=Action_24.inf");
	truclient_step("28", "Click on Cerrar image", "snapshot=Action_28.inf");
	truclient_step("30", "Wait until Mis clientes potenciales... button exists", "snapshot=Action_30.inf");
	lr_start_transaction("CRM_PRD_MX_02_Ventas");
	truclient_step("36", "Click on Cambiar a otra aplicaci�n JavaScript link", "snapshot=Action_36.inf");
	truclient_step("37", "Click on Centro de ventas", "snapshot=Action_37.inf");
	truclient_step("38", "Wait until Oportunidades listitem exists", "snapshot=Action_38.inf");
	lr_start_transaction("CRM_PRD_MX_02.01_ClickOportunidades");
	truclient_step("39", "Click on Oportunidades", "snapshot=Action_39.inf");
	lr_end_transaction("CRM_PRD_MX_02.01_ClickOportunidades",0);
	truclient_step("40", "Wait until Seleccionar vista exists", "snapshot=Action_40.inf");
	truclient_step("41", "Wait 1 seconds", "snapshot=Action_41.inf");
	lr_end_transaction("CRM_PRD_MX_02_Ventas",0);
	truclient_step("42", "Wait until Select a view exists", "snapshot=Action_42.inf");
	lr_start_transaction("CRM_PRD_MX_03_SearchOportunidad");
	truclient_step("43", "Wait 1 milliseconds", "snapshot=Action_43.inf");
	lr_start_transaction("CRM_PRD_MX_03.01_EnterSearchCriteria");
	truclient_step("44", "Click on Buscar registros textbox", "snapshot=Action_44.inf");
	truclient_step("45", "Type HTTP WATCH in Buscar registros textbox", "snapshot=Action_45.inf");
	truclient_step("46", "Click on Iniciar b�squeda", "snapshot=Action_46.inf");
	lr_end_transaction("CRM_PRD_MX_03.01_EnterSearchCriteria",0);
	truclient_step("49", "Wait until HTTP WATCH TEST gridcell exists", "snapshot=Action_49.inf");
	lr_start_transaction("CRM_PRD_MX_03.02_SelectOportunityResult");
	truclient_step("51", "Click on HTTP WATCH TEST", "snapshot=Action_51.inf");
	truclient_step("53", "Wait until Business Process FlowCompletado... exists", "snapshot=Action_53.inf");
	lr_end_transaction("CRM_PRD_MX_03.02_SelectOportunityResult",0);
	truclient_step("55", "Wait 1 milliseconds", "snapshot=Action_55.inf");
	lr_end_transaction("CRM_PRD_MX_03_SearchOportunidad",0);
	truclient_step("56", "Wait 1 seconds", "snapshot=Action_56.inf");
	truclient_step("57", "Click on 1. Prospectar label", "snapshot=Action_57.inf");
	truclient_step("59", "Click on Ancla el control flotante... label", "snapshot=Action_59.inf");
	truclient_step("61", "Wait until Completado label exists", "snapshot=Action_61.inf");
	lr_start_transaction("CRM_PRD_MX_04_DisplayOportunidad");
	truclient_step("62", "Wait 1 milliseconds", "snapshot=Action_62.inf");
	lr_start_transaction("CRM_PRD_MX_04.01_DisplayDatosContacto");
	truclient_step("64", "Click on Ernesto Garza label", "snapshot=Action_64.inf");
	truclient_step("66", "Wait until Contact: ContactErnesto... exists", "snapshot=Action_66.inf");
	lr_end_transaction("CRM_PRD_MX_04.01_DisplayDatosContacto",0);
	truclient_step("67", "Wait 5 seconds", "snapshot=Action_67.inf");
	lr_start_transaction("CRM_PRD_MX_04.02_ReturnOportunidad");
	truclient_step("68", "Go Back a page", "snapshot=Action_68.inf");
	lr_end_transaction("CRM_PRD_MX_04.02_ReturnOportunidad",0);
	truclient_step("70", "If An error has been occurred.... exists", "snapshot=Action_70.inf");
	{
		truclient_step("70.2", "Click on Aceptar", "snapshot=Action_70.2.inf");
		truclient_step("70.3", "Click on Aceptar", "snapshot=Action_70.3.inf");
	}
	truclient_step("71", "Wait until Oportunidad: Opportunity... exists", "snapshot=Action_71.inf");
	truclient_step("72", "Click on 1. Prospectar label", "snapshot=Action_72.inf");
	truclient_step("73", "Wait until Completado label exists", "snapshot=Action_73.inf");
	lr_start_transaction("CRM_PRD_MX_04.03_ClickObra");
	truclient_step("74", "Click on HTTP WATCH TEST label", "snapshot=Action_74.inf");
	truclient_step("75", "Wait 1 seconds", "snapshot=Action_75.inf");
	truclient_step("76", "If �Desea abandonar esta... heading exists", "snapshot=Action_76.inf");
	{
		truclient_step("76.2", "Click on Aceptar", "snapshot=Action_76.2.inf");
	}
	truclient_step("77", "Wait until 7180 label exists", "snapshot=Action_77.inf");
	lr_end_transaction("CRM_PRD_MX_04.03_ClickObra",0);
	truclient_step("78", "Wait 3 seconds", "snapshot=Action_78.inf");
	lr_start_transaction("CRM_PRD_MX_04.04_ClickDatosdeVentas7180");
	truclient_step("90", "Double click on 7180 label", "snapshot=Action_90.inf");
	truclient_step("92", "Wait 3 seconds", "snapshot=Action_92.inf");
	truclient_step("93", "Wait until Datos Comerciales de la ...Obra Informacion exists", "snapshot=Action_93.inf");
	truclient_step("94", "Wait 3 milliseconds", "snapshot=Action_94.inf");
	lr_end_transaction("CRM_PRD_MX_04.04_ClickDatosdeVentas7180",0);
	truclient_step("95", "Wait 3 seconds", "snapshot=Action_95.inf");
	lr_start_transaction("CRM_PRD_MX_04.05_ReturnDatosdeObra");
	truclient_step("96", "Go Back a page", "snapshot=Action_96.inf");
	lr_end_transaction("CRM_PRD_MX_04.05_ReturnDatosdeObra",0);
	truclient_step("97", "Wait 3 seconds", "snapshot=Action_97.inf");
	truclient_step("98", "Wait until Datos Comerciales Genera...?Nombre*Sales... exists", "snapshot=Action_98.inf");
	truclient_step("99", "Wait until Datos Generales Sucursal/... heading exists", "snapshot=Action_99.inf");
	lr_start_transaction("CRM_PRD_MX_04.06_ReturnOportunidad");
	truclient_step("101", "Go Back a page", "snapshot=Action_101.inf");
	lr_end_transaction("CRM_PRD_MX_04.06_ReturnOportunidad",0);
	truclient_step("102", "Wait 3 seconds", "snapshot=Action_102.inf");
	truclient_step("103", "Wait until HTTP WATCH TEST heading exists", "snapshot=Action_103.inf");
	truclient_step("104", "Wait 1 seconds", "snapshot=Action_104.inf");
	truclient_step("105", "Go Back a page", "snapshot=Action_105.inf");
	truclient_step("106", "Click on 1. Prospectar label", "snapshot=Action_106.inf");
	truclient_step("107", "Wait until Completado label exists", "snapshot=Action_107.inf");
	lr_start_transaction("CRM_PRD_MX_04.07_DisplayCliente");
	truclient_step("108", "Click on TEST HTTP WATCH label", "snapshot=Action_108.inf");
	truclient_step("109", "Wait until Nombre 1*TEST HTTP WATCH exists", "snapshot=Action_109.inf");
	lr_end_transaction("CRM_PRD_MX_04.07_DisplayCliente",0);
	truclient_step("110", "Wait 1 seconds", "snapshot=Action_110.inf");
	lr_start_transaction("CRM_PRD_MX_04.08_ReturnOportunidad");
	truclient_step("111", "Go Back a page", "snapshot=Action_111.inf");
	lr_end_transaction("CRM_PRD_MX_04.08_ReturnOportunidad",0);
	truclient_step("112", "Wait 1 seconds", "snapshot=Action_112.inf");
	truclient_step("113", "Wait until HTTP WATCH TEST heading exists", "snapshot=Action_113.inf");
	truclient_step("115", "Wait 1 seconds", "snapshot=Action_115.inf");
	truclient_step("118", "Mouse down on Entity: Opportunity,...", "snapshot=Action_118.inf");
	truclient_step("119", "Click on 3. Presentar Propuesta... label", "snapshot=Action_119.inf");
	truclient_step("120", "Wait until 3. Presentar Propuesta... listbox exists", "snapshot=Action_120.inf");
	lr_start_transaction("CRM_PRD_MX_04.09_DisplayQuotation");
	truclient_step("122", "Click on HTTP WATCH TEST label", "snapshot=Action_122.inf");
	truclient_step("124", "Wait until Oportunidad*HTTP WATCH... exists", "snapshot=Action_124.inf");
	lr_end_transaction("CRM_PRD_MX_04.09_DisplayQuotation",0);
	truclient_step("125", "Wait 2 seconds", "snapshot=Action_125.inf");
	truclient_step("126", "Wait until Mostrar Productos exists", "snapshot=Action_126.inf");
	lr_start_transaction("CRM_PRD_MX_04.10_DisplayProducts");
	truclient_step("128", "Click on Mostrar Productos", "snapshot=Action_128.inf");
	truclient_step("129", "Wait until 4 20000005 1 100 1 28... exists", "snapshot=Action_129.inf");
	lr_end_transaction("CRM_PRD_MX_04.10_DisplayProducts",0);
	truclient_step("130", "For ( var i = 0 ; i < 10 ; i++ )", "snapshot=Action_130.inf");
	{
		truclient_step("130.1", "Wait 3 seconds", "snapshot=Action_130.1.inf");
		truclient_step("130.2", "If spinner object displaying exists", "snapshot=Action_130.2.inf");
		{
			truclient_step("130.2.1", "Wait 3 seconds", "snapshot=Action_130.2.1.inf");
		}
		truclient_step("Else");
		{
			truclient_step("130.2.1", "If element (5) exists", "snapshot=Action_130.2.1.inf");
			{
				truclient_step("130.2.1.1", "Break", "snapshot=Action_130.2.1.1.inf");
			}
			truclient_step("Else");
			{
				truclient_step("130.2.1.1", "Continue", "snapshot=Action_130.2.1.1.inf");
			}
			truclient_step("130.2.2", "If Product Information exists", "snapshot=Action_130.2.2.inf");
			{
				truclient_step("130.2.2.1", "Break", "snapshot=Action_130.2.2.1.inf");
			}
			truclient_step("Else");
			{
				truclient_step("130.2.2.1", "Continue", "snapshot=Action_130.2.2.1.inf");
			}
		}
	}
	lr_start_transaction("CRM_PRD_MX_04.11_ReturnOportunidad");
	truclient_step("132", "Go Back a page", "snapshot=Action_132.inf");
	lr_end_transaction("CRM_PRD_MX_04.11_ReturnOportunidad",0);
	truclient_step("133", "Wait until HTTP WATCH TEST heading exists", "snapshot=Action_133.inf");
	truclient_step("134", "Wait 1 milliseconds", "snapshot=Action_134.inf");
	lr_end_transaction("CRM_PRD_MX_04_DisplayOportunidad",0);
	lr_start_transaction("CRM_PRD_MX_05_Logoff");
	truclient_step("135", "Click on Informaci�n del usuario...", "snapshot=Action_135.inf");
	truclient_step("136", "Wait 1 milliseconds", "snapshot=Action_136.inf");
	truclient_step("138", "Click on Cerrar sesi�n", "snapshot=Action_138.inf");
	truclient_step("139", "Wait until You signed out of your... heading exists", "snapshot=Action_139.inf");
	lr_end_transaction("CRM_PRD_MX_05_Logoff",0);

	return 0;
}
