(function() {
	"use strict";

	function actionAction() {
		TCS.transaction.start("CRM_PRD_EG_00_LoadInitPage");
		// Navigate to "http://eucldyndbp01.crm4.dynamics.com/"
		TCS.browser.navigate({
			"Location": TCS.argType.JSArg("\"http://eucldyndbp01.crm4.dynamics.com/\""),
			"Step Min Time": 3,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_00_LoadInitPage", "Auto");
		TCS.transaction.start("CRM_PRD_EG_01_Login");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.start("CRM_PRD_EG_01.01_EnterUserName");
		// Type mxsrperformance@cemex.onmicrosoft.com in Sign in Can’t access... emailbox
		TCS.object.tcManaged.signinCantaccessObject.type({
			"Value": "mxsrperformance@cemex.onmicrosoft.com",
			"Typing Interval": 150,
			"Step Min Time": 1,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Press key Enter on Sign in Can’t access... emailbox
		TCS.object.tcManaged.signinCantaccessObject.pressKey({
			"Key Name": "Enter",
			"Step Min Time": 1,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_01.01_EnterUserName", "Auto");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.start("CRM_PRD_EG_01.02_EnterPassword");
		// Wait until mxsrperformance@cemex.onmicrosoft.com... passwordbox exists
		TCS.object.tcManaged.mxsrperformancecemexonmicrosoftcomObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Type ********* in mxsrperformance@cemex.onmicrosoft.com... passwordbox
		TCS.object.tcManaged.mxsrperformancecemexonmicrosoftcomObject_2.type({
			"Value": TCS.argType.Base64Arg("Q2hlbWEzMjAx"),
			"End Event": TCS.argType.endEvent.actionCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_01.02_EnterPassword", "Auto");
		TCS.transaction.start("CRM_PRD_EG_01.03_EnterSignInButton");
		// Click on Sign in button
		TCS.object.tcManaged.signinObject.click({
			"X Coordinate": 32,
			"Y Coordinate": 6,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_01.03_EnterSignInButton", "Auto");
		TCS.transaction.start("CRM_PRD_EG_01.04_EnterYesButton");
		// Click on Yes button
		TCS.object.tcManaged.yesObject.click({
			"X Coordinate": 31,
			"Y Coordinate": 9,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_01.04_EnterYesButton", "Auto");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.end("CRM_PRD_EG_01_Login", "Auto");
		// Wait until Sales JavaScript link exists
		TCS.object.tcManaged.salesObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.start("CRM_PRD_EG_02_Ventas");
		// Click on Sales JavaScript link
		TCS.object.tcManaged.salesObject_2.click({
			"X Coordinate": 12,
			"Y Coordinate": 11,
			"Step Min Time": 0,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.start("CRM_PRD_EG_02.01_ClickOportunidades");
		// Click on Opportunities link
		TCS.object.tcManaged.opportunitiesObject.click({
			"X Coordinate": 7,
			"Y Coordinate": 12,
			"End Event": TCS.argType.endEvent.actionCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_02.01_ClickOportunidades", "Auto");
		// Wait 10 milliseconds
		TCS.utils.wait({
			"Interval": 10,
			"Unit": "Milliseconds"
		});
		TCS.transaction.end("CRM_PRD_EG_02_Ventas", "Auto");
		// Wait until Search for records label exists
		TCS.object.tcManaged.searchforrecordsObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.start("CRM_PRD_EG_03_SearchOportunidad");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.start("CRM_PRD_EG_03.01_EnterSearchCriteria");
		// Type SMOKE TEST S23 RMX S3 in Search for records textbox
		TCS.object.tcManaged.searchforrecordsObject_2.type({
			"Value": "SMOKE TEST S23 RMX S3",
			"End Event": TCS.argType.endEvent.actionCompleted
		});
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		// Click on Start search 
		TCS.object.tcManaged.startsearchObject.click({
			"X Coordinate": 11,
			"Y Coordinate": 11,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_03.01_EnterSearchCriteria", "Auto");
		TCS.transaction.start("CRM_PRD_EG_03.02_SelectOportunityResult");
		// Click on SMOKE TEST S23 RMX S3 JavaScript link
		TCS.object.tcManaged.sMOKETESTS23RMXS3Object.click({
			"X Coordinate": 21,
			"Y Coordinate": 8,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait until SMOKE TEST S23 RMX S3 heading exists
		TCS.object.tcManaged.sMOKETESTSC2RMXObject_2.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_03.02_SelectOportunityResult", "Auto");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.end("CRM_PRD_EG_03_SearchOportunidad", "Auto");
		// Click on Entity: Opportunity,... 
		TCS.object.tcManaged.entityOpportunityObject.click({
			"X Coordinate": 52,
			"Y Coordinate": 16,
			"End Event": TCS.argType.endEvent.syncNetworkCompleted
		});
		TCS.transaction.start("CRM_PRD_EG_04_DisplayOportunidad");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.start("CRM_PRD_EG_04.01_DisplayDatosContacto");
		// Click on SMOKE IKRAM 
		TCS.object.tcManaged.sMOKEIKRAMObject.click({
			"End Event": TCS.argType.endEvent.actionCompleted
		});
		// Wait until Contact  exists
		TCS.object.tcManaged.contactObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.01_DisplayDatosContacto", "Auto");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.start("CRM_PRD_EG_04.02_ReturnOportunidad");
		// Go Back a page
		TCS.browser.goBack({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait until SMOKE TEST S23 RMX S3 heading exists
		TCS.object.tcManaged.sMOKETESTSC2RMXObject_2.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.02_ReturnOportunidad", "Auto");
		// Wait 1 milliseconds
		// **** Warning - Ignoring original end event of the "wait" step ****
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.start("CRM_PRD_EG_04.03_ClickObra");
		// Click on SMOKE TEST S23 RMX S3 
		TCS.object.tcManaged.sMOKETESTS23RMXS3Object_2.click({
			"X Coordinate": 41,
			"Y Coordinate": 10,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait until Jobsite General Data heading exists
		TCS.object.tcManaged.jobsiteGeneralDataObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.03_ClickObra", "Auto");
		// Wait 1 seconds
		TCS.utils.wait({
			"Interval": 1
		});
		TCS.transaction.start("CRM_PRD_EG_04.04_ClickDatosdeVentas4521");
		// Click on See the records associated... JavaScript link
		TCS.object.tcManaged.seetherecordsassociatedObject.click({
			"X Coordinate": 6,
			"Y Coordinate": 4,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Click on Sales Data SMOKE TEST... JavaScript link
		TCS.object.tcManaged.salesDataSMOKETESTObject_2.click({
			"X Coordinate": 80,
			"Y Coordinate": 5,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait until General Sales Data heading exists
		TCS.object.tcManaged.generalSalesDataObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.04_ClickDatosdeVentas4521", "Auto");
		// Wait 1 seconds
		TCS.utils.wait({
			"Interval": 1
		});
		TCS.transaction.start("CRM_PRD_EG_04.05_ReturnDatosdeObra");
		// Go Back a page
		TCS.browser.goBack({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait until Jobsite General Data heading exists
		TCS.object.tcManaged.jobsiteGeneralDataObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.05_ReturnDatosdeObra", "Auto");
		TCS.transaction.start("CRM_PRD_EG_04.06_ReturnOportunidad");
		// Go Back a page
		TCS.browser.goBack({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait until SMOKE TEST S23 RMX S3 heading exists
		TCS.object.tcManaged.sMOKETESTSC2RMXObject_2.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.06_ReturnOportunidad", "Auto");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.start("CRM_PRD_EG_04.07_DisplayCliente");
		// Click on SMOKE TEST IKRAM 
		TCS.object.tcManaged.sMOKETESTIKRAMObject.click({
			"X Coordinate": 24,
			"Y Coordinate": 4,
			"Object Timeout": 60,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait until Display accont Data  exists
		TCS.object.tcManaged.displayaccontDataObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.07_DisplayCliente", "Auto");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.start("CRM_PRD_EG_04.08_ReturnOportunidad");
		// Go Back a page
		TCS.browser.goBack({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		// Wait until SMOKE TEST S23 RMX S3 heading exists
		TCS.object.tcManaged.sMOKETESTSC2RMXObject_2.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.08_ReturnOportunidad", "Auto");
		// Wait 1 seconds
		TCS.utils.wait({
			"Interval": 1
		});
		TCS.transaction.start("CRM_PRD_EG_04.09_DisplayQuotation");
		// Click on Entity: Opportunity,... 
		TCS.object.tcManaged.entityOpportunityObject_2.click({
			"X Coordinate": 20,
			"Y Coordinate": 12,
			"End Event": TCS.argType.endEvent.actionCompleted
		});
		// Wait until Quote  exists
		TCS.object.tcManaged.quoteObject_2.wait({
			"End Event": TCS.argType.endEvent.actionCompleted
		});
		// Click on SMOKE TEST S23 RMX S1 
		TCS.object.tcManaged.sMOKETESTS23RMXS1Object.click({
			"End Event": TCS.argType.endEvent.actionCompleted
		});
		// Wait until Quote : North Europe...  exists
		TCS.object.tcManaged.quoteNorthEuropeObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.09_DisplayQuotation", "Auto");
		TCS.transaction.start("CRM_PRD_EG_04.10_DisplayProducts");
		// Click on Show Products 
		TCS.object.tcManaged.showProductsObject.click({
			"X Coordinate": 32,
			"Y Coordinate": 3,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// For (var i = 0; i < 10; i++)
		for (TCS.utils.evalInBrowser("var i = 0"); TCS.utils.evalInBrowser("i < 10"); TCS.utils.evalInBrowser("i++")) {
			// If spinner object displaying  exists
			var localVar0 = TCS.object.tcManaged.spinnerobjectdisplayingObject.isExist({
				"Object Timeout": 2
			});
			if (localVar0) {
				// Wait 2 seconds
				TCS.utils.wait({
					"Interval": 2
				});
			} else {
				// Evaluate JavaScript code var i=10
				TCS.utils.evalInBrowser("var i=10");
			}
		}
		TCS.transaction.end("CRM_PRD_EG_04.10_DisplayProducts", "Auto");
		TCS.transaction.start("CRM_PRD_EG_04.11_ReturnOportunidad");
		// Go Back a page
		TCS.browser.goBack({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait until SMOKE TEST S23 RMX S3 heading exists
		TCS.object.tcManaged.sMOKETESTSC2RMXObject_2.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_04.11_ReturnOportunidad", "Auto");
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		TCS.transaction.end("CRM_PRD_EG_04_DisplayOportunidad", "Auto");
		TCS.transaction.start("CRM_PRD_EG_05_Logoff");
		// Click on MX Sr Performance EUCLDYN... 
		TCS.object.tcManaged.mXSrPerformanceEUCLDYNObject.click({
			"X Coordinate": 13,
			"Y Coordinate": 29,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait 1 milliseconds
		TCS.utils.wait({
			"Interval": 1,
			"Unit": "Milliseconds"
		});
		// Click on Sign out JavaScript link
		TCS.object.tcManaged.signoutObject.click({
			"X Coordinate": 46,
			"Y Coordinate": 21,
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		// Wait until You signed out of your... heading exists
		TCS.object.tcManaged.yousignedoutofyourObject.wait({
			"End Event": TCS.argType.endEvent.stepNetworkCompleted
		});
		TCS.transaction.end("CRM_PRD_EG_05_Logoff", "Auto");
	}
	actionAction();

})();